package com.example.onvandroid;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;

public class MainActivity extends AppCompatActivity {

    String[] mode = {"Length", "Weight", "Temperature", "Time"};
    String[] modeLengthFrom = {"Meters", "Miles", "Yards"};
    String[] modeLengthTo = {"Meters", "Miles", "Yards"};
    String[] modeWeightFrom = {"KG", "LBS", "ST"};
    String[] modeWeightTo = {"KG", "LBS", "ST"};
    String[] modeTemperatureFrom = {"Kelvin", "Celsius", "Fahrenheit"};
    String[] modeTemperatureTo = {"Kelvin", "Celsius", "Fahrenheit"};
    String[] modeTimeFrom = {"Second", "Minute", "Hour"};
    String[] modeTimeTo = {"Second", "Minute", "Hour"};
    Spinner lengthFrom;
    Spinner lengthTo;
    Button convertLength;
    Spinner weightFrom;
    Spinner weightTo;
    Button convertWeight;
    Spinner temperatureFrom;
    Spinner temperatureTo;
    Button convertTemperature;
    EditText myInputTextFrom;
    EditText myInputTextTo;
    Spinner timeFrom;
    Spinner timeTo;
    Button convertTime;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        myInputTextFrom = (EditText) findViewById(R.id.inputTextFrom);
        myInputTextTo = (EditText) findViewById(R.id.inputTextTo);
        myInputTextTo.setEnabled(false);

        //Создание ОСНОВНОГО выпадающего списка
        Spinner spinner = (Spinner) findViewById(R.id.cities);


        //Подсписки
        lengthFrom = (Spinner) findViewById(R.id.LengthFrom);
        lengthTo = (Spinner) findViewById(R.id.LengthTo);
        convertLength = (Button) findViewById(R.id.Convert);
        weightFrom = (Spinner) findViewById(R.id.weightFrom);
        weightTo = (Spinner) findViewById(R.id.weightTo);
        convertWeight = (Button) findViewById(R.id.convertWeight);
        temperatureFrom = (Spinner) findViewById(R.id.TemperatureFrom);
        temperatureTo = (Spinner) findViewById(R.id.TemperatureTo);
        convertTemperature = (Button) findViewById(R.id.convertTemperature);
        timeFrom = (Spinner) findViewById(R.id.timeFrom);
        timeTo = (Spinner) findViewById(R.id.timeTo);
        convertTime = (Button) findViewById(R.id.convertTime);

        //Клонирования индекса элемента из принимающего подсписка длины LENGTH
        ArrayAdapter<String> adapterLengthFrom = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, modeLengthFrom);
        adapterLengthFrom.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        lengthFrom.setAdapter(adapterLengthFrom);

        //Клонирования индекса элемента из отдающего подсписка длины LENGTH
        ArrayAdapter<String> adapterLengthTo = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, modeLengthTo);
        adapterLengthTo.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        lengthTo.setAdapter(adapterLengthTo);

        //Клонирования индекса элемента из принимающего подсписка длины WEIGHT
        ArrayAdapter<String> adapterWeightFrom = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, modeWeightFrom);
        adapterWeightFrom.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        weightFrom.setAdapter(adapterWeightFrom);

        //Клонирования индекса элемента из отдающего подсписка длины WEIGHT
        ArrayAdapter<String> adapterWeightTo = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, modeWeightTo);
        adapterWeightTo.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        weightTo.setAdapter(adapterWeightTo);

        //Клонирования индекса элемента из принимающего подсписка длины TEMPERATURE
        ArrayAdapter<String> adapterTemperatureFrom = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, modeTemperatureFrom);
        adapterTemperatureFrom.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        temperatureFrom.setAdapter(adapterTemperatureFrom);

        //Клонирования индекса элемента из отдающего подсписка длины TEMPERATURE
        ArrayAdapter<String> adapterTemperatureTo = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, modeTemperatureTo);
        adapterTemperatureTo.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        temperatureTo.setAdapter(adapterTemperatureTo);

        //Клонирования индекса элемента из принимающего подсписка длины LENGTH
        ArrayAdapter<String> adapterTimeFrom = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, modeTimeFrom);
        adapterTimeFrom.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        timeFrom.setAdapter(adapterTimeFrom);

        //Клонирования индекса элемента из отдающего подсписка длины LENGTH
        ArrayAdapter<String> adapterTimeTo = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, modeTimeTo);
        adapterTimeTo.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        timeTo.setAdapter(adapterTimeTo);

        //Клонирования индекса элемента из ОСНОВНОГО списка
        ArrayAdapter<String> adapterMode = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, mode);
        adapterMode.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapterMode);
//
        //Клонирования индекса элемента из принимающего подсписка длины


        //Обработка при нажатии
        AdapterView.OnItemSelectedListener itemSelectedMode = new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                // Получаем выбранный элемент из списка
                String item = (String) parent.getItemAtPosition(position);

                if (item.equals("Length")) {
                    timeFrom.setVisibility(View.INVISIBLE);
                    timeTo.setVisibility(View.INVISIBLE);
                    convertTime.setVisibility(View.INVISIBLE);
                    weightFrom.setVisibility(View.INVISIBLE);
                    weightTo.setVisibility(View.INVISIBLE);
                    convertWeight.setVisibility(View.INVISIBLE);
                    temperatureFrom.setVisibility(View.INVISIBLE);
                    temperatureTo.setVisibility(View.INVISIBLE);
                    convertTemperature.setVisibility(View.INVISIBLE);
                    lengthFrom.setVisibility(View.VISIBLE);
                    lengthTo.setVisibility(View.VISIBLE);
                    convertLength.setVisibility(View.VISIBLE);
                } else if (item.equals("Weight")) {
                    timeFrom.setVisibility(View.INVISIBLE);
                    timeTo.setVisibility(View.INVISIBLE);
                    convertTime.setVisibility(View.INVISIBLE);
                    lengthFrom.setVisibility(View.INVISIBLE);
                    lengthTo.setVisibility(View.INVISIBLE);
                    convertLength.setVisibility(View.INVISIBLE);
                    temperatureFrom.setVisibility(View.INVISIBLE);
                    temperatureTo.setVisibility(View.INVISIBLE);
                    convertTemperature.setVisibility(View.INVISIBLE);
                    weightFrom.setVisibility(View.VISIBLE);
                    weightTo.setVisibility(View.VISIBLE);
                    convertWeight.setVisibility(View.VISIBLE);
                } else if (item.equals("Temperature")) {
                    timeFrom.setVisibility(View.INVISIBLE);
                    timeTo.setVisibility(View.INVISIBLE);
                    convertTime.setVisibility(View.INVISIBLE);
                    lengthFrom.setVisibility(View.INVISIBLE);
                    lengthTo.setVisibility(View.INVISIBLE);
                    convertLength.setVisibility(View.INVISIBLE);
                    weightFrom.setVisibility(View.INVISIBLE);
                    weightTo.setVisibility(View.INVISIBLE);
                    convertWeight.setVisibility(View.INVISIBLE);
                    temperatureFrom.setVisibility(View.VISIBLE);
                    temperatureTo.setVisibility(View.VISIBLE);
                    convertTemperature.setVisibility(View.VISIBLE);
                } else if (item.equals("Time")) {
                    lengthFrom.setVisibility(View.INVISIBLE);
                    lengthTo.setVisibility(View.INVISIBLE);
                    convertLength.setVisibility(View.INVISIBLE);
                    weightFrom.setVisibility(View.INVISIBLE);
                    weightTo.setVisibility(View.INVISIBLE);
                    convertWeight.setVisibility(View.INVISIBLE);
                    temperatureFrom.setVisibility(View.INVISIBLE);
                    temperatureTo.setVisibility(View.INVISIBLE);
                    convertTemperature.setVisibility(View.INVISIBLE);
                    timeFrom.setVisibility(View.VISIBLE);
                    timeTo.setVisibility(View.VISIBLE);
                    convertTime.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        };
        spinner.setOnItemSelectedListener(itemSelectedMode);
    }

    public void Converte(View view) {
        try {
            String getConvertFrom = lengthFrom.getSelectedItem().toString();
            String getConvertTo = lengthTo.getSelectedItem().toString();
            if (getConvertFrom.equals("Meters") && getConvertTo.equals("Meters")) {
                myInputTextTo.setText(myInputTextFrom.getText());
            } else if (getConvertFrom.equals("Yards") && getConvertTo.equals("Yards")) {
                myInputTextTo.setText(myInputTextFrom.getText());
            } else if (getConvertFrom.equals("Miles") && getConvertTo.equals("Miles")) {
                myInputTextTo.setText(myInputTextFrom.getText());
            } else if (getConvertFrom.equals("Meters") && getConvertTo.equals("Yards")) {
                double convert = Integer.parseInt(String.valueOf(myInputTextFrom.getText()));
                myInputTextTo.setText(String.valueOf(convert * 1.094));
            } else if (getConvertFrom.equals("Meters") && getConvertTo.equals("Miles")) {
                double convert = Integer.parseInt(String.valueOf(myInputTextFrom.getText()));
                myInputTextTo.setText(String.valueOf(convert / 1609));
            } else if (getConvertFrom.equals("Yards") && getConvertTo.equals("Meters")) {
                double convert = Integer.parseInt(String.valueOf(myInputTextFrom.getText()));
                myInputTextTo.setText(String.valueOf(convert / 1.094));
            } else if (getConvertFrom.equals("Yards") && getConvertTo.equals("Miles")) {
                double convert = Integer.parseInt(String.valueOf(myInputTextFrom.getText()));
                myInputTextTo.setText(String.valueOf(convert / 1760));
            } else if (getConvertFrom.equals("Miles") && getConvertTo.equals("Meters")) {
                double convert = Integer.parseInt(String.valueOf(myInputTextFrom.getText()));
                myInputTextTo.setText(String.valueOf(convert * 1609));
            } else if (getConvertFrom.equals("Miles") && getConvertTo.equals("Yards")) {
                double convert = Integer.parseInt(String.valueOf(myInputTextFrom.getText()));
                myInputTextTo.setText(String.valueOf(convert * 1760));
            }
        } catch (Exception e) {
            myInputTextTo.setText("Invalid input!");
        }
    }

    public void ConverteWeight(View view) {
        try {
            String getConvertFrom = weightFrom.getSelectedItem().toString();
            String getConvertTo = weightTo.getSelectedItem().toString();
            if (getConvertFrom.equals("KG") && getConvertTo.equals("KG")) {
                myInputTextTo.setText(myInputTextFrom.getText());
            } else if (getConvertFrom.equals("LBS") && getConvertTo.equals("LBS")) {
                myInputTextTo.setText(myInputTextFrom.getText());
            } else if (getConvertFrom.equals("ST") && getConvertTo.equals("ST")) {
                myInputTextTo.setText(myInputTextFrom.getText());
            } else if (getConvertFrom.equals("KG") && getConvertTo.equals("LBS")) {
                double convert = Integer.parseInt(String.valueOf(myInputTextFrom.getText()));
                myInputTextTo.setText(String.valueOf(convert * 2.205));
            } else if (getConvertFrom.equals("KG") && getConvertTo.equals("ST")) {
                double convert = Integer.parseInt(String.valueOf(myInputTextFrom.getText()));
                myInputTextTo.setText(String.valueOf(convert / 907));
            } else if (getConvertFrom.equals("LBS") && getConvertTo.equals("KG")) {
                double convert = Integer.parseInt(String.valueOf(myInputTextFrom.getText()));
                myInputTextTo.setText(String.valueOf(convert / 2.205));
            } else if (getConvertFrom.equals("LBS") && getConvertTo.equals("ST")) {
                double convert = Integer.parseInt(String.valueOf(myInputTextFrom.getText()));
                myInputTextTo.setText(String.valueOf(convert / 2000));
            } else if (getConvertFrom.equals("ST") && getConvertTo.equals("KG")) {
                double convert = Integer.parseInt(String.valueOf(myInputTextFrom.getText()));
                myInputTextTo.setText(String.valueOf(convert * 907));
            } else if (getConvertFrom.equals("ST") && getConvertTo.equals("LBS")) {
                double convert = Integer.parseInt(String.valueOf(myInputTextFrom.getText()));
                myInputTextTo.setText(String.valueOf(convert * 2000));
            }
        } catch (Exception e) {
            myInputTextTo.setText("Invalid input!");
        }
    }

    public void ConverteTemperature(View view) {
        try {
            String getConvertFrom = temperatureFrom.getSelectedItem().toString();
            String getConvertTo = temperatureTo.getSelectedItem().toString();
            if (getConvertFrom.equals("kelvin") && getConvertTo.equals("kelvin")) {
                myInputTextTo.setText(myInputTextFrom.getText());
            } else if (getConvertFrom.equals("Celsius") && getConvertTo.equals("Celsius")) {
                myInputTextTo.setText(myInputTextFrom.getText());
            } else if (getConvertFrom.equals("Fahrenheit") && getConvertTo.equals("Fahrenheit")) {
                myInputTextTo.setText(myInputTextFrom.getText());
            } else if (getConvertFrom.equals("kelvin") && getConvertTo.equals("Celsius")) {
                double convert = Integer.parseInt(String.valueOf(myInputTextFrom.getText()));
                myInputTextTo.setText(String.valueOf(convert - 273.15));
            } else if (getConvertFrom.equals("kelvin") && getConvertTo.equals("Fahrenheit")) {
                double convert = Integer.parseInt(String.valueOf(myInputTextFrom.getText()));
                myInputTextTo.setText(String.valueOf((convert - 273.15) * 9 / 5 + 32));
            } else if (getConvertFrom.equals("Celsius") && getConvertTo.equals("kelvin")) {
                double convert = Integer.parseInt(String.valueOf(myInputTextFrom.getText()));
                myInputTextTo.setText(String.valueOf(convert + 273.15));
            } else if (getConvertFrom.equals("Celsius") && getConvertTo.equals("Fahrenheit")) {
                double convert = Integer.parseInt(String.valueOf(myInputTextFrom.getText()));
                myInputTextTo.setText(String.valueOf((convert * 9 / 5) + 32));
            } else if (getConvertFrom.equals("Fahrenheit") && getConvertTo.equals("Celsius")) {
                double convert = Integer.parseInt(String.valueOf(myInputTextFrom.getText()));
                myInputTextTo.setText(String.valueOf((convert - 32) * 5 / 9));
            } else if (getConvertFrom.equals("Fahrenheit") && getConvertTo.equals("kelvin")) {
                double convert = Integer.parseInt(String.valueOf(myInputTextFrom.getText()));
                myInputTextTo.setText(String.valueOf((convert - 32) * 5 / 9 + 273.15));
            }
        } catch (Exception e) {
            myInputTextTo.setText("Invalid input!");
        }
    }

    public void ConverteTime(View view) {
        try {
            String getConvertFrom = timeFrom.getSelectedItem().toString();
            String getConvertTo = timeTo.getSelectedItem().toString();
            if (getConvertFrom.equals("Second") && getConvertTo.equals("Second")) {
                myInputTextTo.setText(myInputTextFrom.getText());
            } else if (getConvertFrom.equals("Minute") && getConvertTo.equals("Minute")) {
                myInputTextTo.setText(myInputTextFrom.getText());
            } else if (getConvertFrom.equals("Hour") && getConvertTo.equals("Hour")) {
                myInputTextTo.setText(myInputTextFrom.getText());
            } else if (getConvertFrom.equals("Second") && getConvertTo.equals("Minute")) {
                double convert = Integer.parseInt(String.valueOf(myInputTextFrom.getText()));
                myInputTextTo.setText(String.valueOf(convert / 60));
            } else if (getConvertFrom.equals("Second") && getConvertTo.equals("Hour")) {
                double convert = Integer.parseInt(String.valueOf(myInputTextFrom.getText()));
                myInputTextTo.setText(String.valueOf(convert / 3600));
            } else if (getConvertFrom.equals("Minute") && getConvertTo.equals("Second")) {
                double convert = Integer.parseInt(String.valueOf(myInputTextFrom.getText()));
                myInputTextTo.setText(String.valueOf(convert * 60));
            } else if (getConvertFrom.equals("Minute") && getConvertTo.equals("Hour")) {
                double convert = Integer.parseInt(String.valueOf(myInputTextFrom.getText()));
                myInputTextTo.setText(String.valueOf(convert / 60));
            } else if (getConvertFrom.equals("Hour") && getConvertTo.equals("Second")) {
                double convert = Integer.parseInt(String.valueOf(myInputTextFrom.getText()));
                myInputTextTo.setText(String.valueOf(convert * 3600));
            } else if (getConvertFrom.equals("Hour") && getConvertTo.equals("Minute")) {
                double convert = Integer.parseInt(String.valueOf(myInputTextFrom.getText()));
                myInputTextTo.setText(String.valueOf(convert * 60));
            }
        } catch (Exception e) {
            myInputTextTo.setText("Invalid input!");
        }
    }
}
